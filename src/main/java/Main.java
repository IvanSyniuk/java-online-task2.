
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * This class calculate probability that everyone at the party will hear the rumor before it stops propagating
 * and calculate an estimate of the expected number of people to hear the rumor.
 *
 * @author Ivan Syniuk
 */

public class Main {

    public static void main(String[] args) {
        final int attempts = 99;
        Scanner sc = new Scanner(System.in);
        System.out.println("Введіть кількість гостей ");


        int numberGuests = sc.nextInt();
        int countAll = 0;
        int people = 0;
        int nextPerson = -1;
        int currentPerson = 1;

        for(int i=0;i<attempts;i++){
            List<Boolean> guests = new ArrayList<Boolean>();
            for(int j = 1; j<=numberGuests;j++){
                guests.add(false);
            }
            guests.set(1 , true);
            boolean heaerded = false;

            while(!heaerded){
                nextPerson = 1 + (int)(Math.random() * (numberGuests-1));
                if(nextPerson == currentPerson){
                    while(nextPerson == currentPerson)
                        nextPerson = 1 + (int)(Math.random() * (numberGuests-1));
                }
                if(guests.get(nextPerson))
                {
                    if(rumorTimes(guests))
                        countAll++;
                    people = people + countPeople(guests);
                    heaerded = true;
                }
                guests.set(nextPerson,true);
                currentPerson = nextPerson;
            }
        }

        System.out.println("Події" + attempts);
        System.out.println("Ймовірність того, що всі почули чутку:" +
                (double)countAll/attempts*100 + "%");
        System.out.println("Середня кількість людей що почули чутку: "+people/attempts);
    }

    /**
     *This method calculate how many people hearded the rumor
     * @param arr
     * @return
     */
    public static int countPeople(List<Boolean> arr){
        int counter = 0;
        for(int i = 1;i<arr.size();i++)
            if(arr.get(i))
                counter++;
        return counter;
    }

    /**
     *This method calculate how many times all guests hearded the rumor
     * @param arr
     * @return
     */
    public static boolean rumorTimes(List<Boolean> arr){
        for(int i = 1;i<arr.size();i++)
            if(!arr.get(i))
                return false;
        return true;
    }
}